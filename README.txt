-- SUMMARY -- 

!This module is currently for Drupal 6.x only!

This module links the module "User Badges" with "Auto Assign Role". With this module the badges on registration get immediately assigned to the user, without a cron run.

It's not necessary to uncomment the last function in user_badges.module (this is online required if you want to add the badges via cron job). This modulu makes this without the cron -> immediately.


-- REQUIREMENTS --

None.


-- DEPENDENCIES --

* User Badges
* Auto Assign Role


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

No need


-- CONTACT --

Current maintainers:
* Matthias Froschmeier (mfrosch) - http://drupal.org/user/932156